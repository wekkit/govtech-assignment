import {
  validateCompany,
  validateApplicant,
  validateEmployee
} from './entityValidators'

window.console.warn = () => {}

describe('validateCompany', () => {
  it('passes a valid company', () => {
    expect(
      validateCompany({ name: 'name', address: 'asd', uen: '12345678X' })
    ).toEqual()
  })

  it('passes a valid company with no uen', () => {
    expect(validateCompany({ name: 'name', address: 'asd' })).toEqual()
  })

  it('returns an error string when no company is passed in', () => {
    expect(validateCompany()).toEqual('Company not found.')
  })

  it('returns an error string when fields are provided', () => {
    expect(validateCompany({})).toEqual(
      'Required fields are missing: name, address'
    )
  })

  it('returns an error string when no name is provided', () => {
    expect(validateCompany({ address: 'hello' })).toEqual(
      'Required fields are missing: name'
    )
  })

  it('returns an error string when no address is provided', () => {
    expect(validateCompany({ name: 'hello' })).toEqual(
      'Required fields are missing: address'
    )
  })

  it('returns an error string when an invalid uen is provided', () => {
    expect(
      validateCompany({ name: 'name', address: 'asd', uen: 'asd' })
    ).toEqual('UEN is not valid: asd')
  })
})

describe('validateApplicant', () => {
  it('passes a valid applicant', () => {
    expect(
      validateApplicant({
        name: 'name',
        phoneNumber: '98374242',
        email: 'he@llo.com'
      })
    ).toEqual()
  })

  it('returns an error string when no applicant is passed in', () => {
    expect(validateApplicant()).toEqual('Applicant not found.')
  })

  it('returns an error string when fields are provided', () => {
    expect(validateApplicant({})).toEqual(
      'Required fields are missing: name, phoneNumber, email'
    )
  })

  it('returns an error string when no name is provided', () => {
    expect(validateApplicant({ phoneNumber: 'hello' })).toEqual(
      'Required fields are missing: name, email'
    )
  })

  it('returns an error string when no phoneNumber is provided', () => {
    expect(validateApplicant({ name: 'hello' })).toEqual(
      'Required fields are missing: phoneNumber, email'
    )
  })

  it('returns an error string when an invalid email is provided', () => {
    expect(
      validateApplicant({ name: 'name', phoneNumber: '98374211', email: 'asd' })
    ).toEqual('Provided email is invalid: asd')
  })

  it('returns an error string when an invalid phoneNumber is provided', () => {
    expect(
      validateApplicant({
        name: 'name',
        phoneNumber: 'asd',
        email: 'asd'
      })
    ).toEqual('Provided phone number is invalid: asd')
  })
})

describe('validateEmployee', () => {
  const valid = {
    name: 'name',
    nric: 'S9145184J',
    passport: 'passport',
    origin: 'country1',
    destination: 'country2',
    datefrom: '12-11-2020',
    dateto: '12-12-2020'
  }
  it('passes a valid applicant', () => {
    expect(validateEmployee(valid)).toEqual()
  })

  it('returns an error string when no employee is passed in', () => {
    expect(validateEmployee()).toEqual('Employee not found.')
  })

  it('returns an error string when no fields are provided', () => {
    expect(validateEmployee({})).toEqual(
      'Required fields are missing: name, passport, origin, destination, datefrom, dateto'
    )
  })

  it('returns an error string when an invalid nric is provided', () => {
    expect(validateEmployee({ ...valid, nric: 'abc' })).toEqual(
      'Provided NRIC is invalid: abc'
    )
  })

  it('returns an error string when an invalid date is provided', () => {
    expect(validateEmployee({ ...valid, datefrom: 'abc' })).toEqual(
      'Provided from date is invalid: abc'
    )
    expect(validateEmployee({ ...valid, dateto: 'abc' })).toEqual(
      'Provided to date is invalid: abc'
    )
  })
})
