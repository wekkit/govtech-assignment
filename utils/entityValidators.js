const validators = require('./valueValidators')

function validateCompany(company) {
  if (!company) return `Company not found.`
  const { name, address, uen } = company

  const missingFields = []
  const isValid = validators.isRequired().func
  if (!isValid(name)) missingFields.push('name')
  if (!isValid(address)) missingFields.push('address')
  if (missingFields.length > 0)
    return `Required fields are missing: ${missingFields.join(', ')}`

  const validUen = validators.isValidUen(null, false).func(uen)
  if (!validUen) return `UEN is not valid: ${uen}`
  return
}

function validateApplicant(applicant) {
  if (!applicant) return `Applicant not found.`
  const { name, phoneNumber, email } = applicant

  const isValid = validators.isRequired().func
  const missingFields = []
  if (!isValid(name)) missingFields.push('name')
  if (!isValid(phoneNumber)) missingFields.push('phoneNumber')
  if (!isValid(email)) missingFields.push('email')
  if (missingFields.length > 0)
    return `Required fields are missing: ${missingFields.join(', ')}`

  const isValidPhoneNumber = validators.isValidPhoneNumber().func
  if (!isValidPhoneNumber(phoneNumber))
    return `Provided phone number is invalid: ${phoneNumber}`

  const isValidEmail = validators.isValidEmail().func
  if (!isValidEmail(email)) return `Provided email is invalid: ${email}`
  return
}

function validateEmployee(employee) {
  if (!employee) return `Employee not found.`
  const {
    name,
    nric,
    passport,
    origin,
    destination,
    datefrom,
    dateto
  } = employee

  const isValid = validators.isRequired().func
  const missingFields = []
  if (!isValid(name)) missingFields.push('name')
  if (!isValid(passport)) missingFields.push('passport')
  if (!isValid(origin)) missingFields.push('origin')
  if (!isValid(destination)) missingFields.push('destination')
  if (!isValid(datefrom)) missingFields.push('datefrom')
  if (!isValid(dateto)) missingFields.push('dateto')
  if (missingFields.length > 0)
    return `Required fields are missing: ${missingFields.join(', ')}`

  const isValidNric = validators.isValidNric().func
  if (!isValidNric(nric)) return `Provided NRIC is invalid: ${nric}`

  const isValidDate = validators.isValidDate().func
  if (!isValidDate(datefrom))
    return `Provided from date is invalid: ${datefrom}`
  if (!isValidDate(dateto)) return `Provided to date is invalid: ${dateto}`
  return
}

module.exports = { validateCompany, validateApplicant, validateEmployee }
