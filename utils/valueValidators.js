const moment = require('./moment.min')

const isRequired = (message = 'This field is required') => ({
  func: val => {
    if (val === 0) return true
    if (val === false) return true
    if (typeof val === 'object') {
      if (Object.keys(val).length === 0) return false
    }
    return Boolean(val)
  },
  message
})

const textMatcher = (
  regex,
  message = 'This field does not match the required format'
) => ({
  func: val => regex.test(val),
  message
})

const isValidNric = (
  message = 'NRIC provided is not valid',
  required = false
) => ({
  func: val => {
    if (!val) return !required

    //Based on http://www.samliew.com/icval/
    if (val.length !== 9) return false
    const firstChr = val.charAt(0)
    let weight = 0
    let characterSet

    if (firstChr === 'F' || firstChr === 'G') {
      characterSet = 'XWUTRQPNMLK'
    } else if (firstChr === 'S' || firstChr === 'T') {
      characterSet = 'JZIHGFEDCBA'
    } else {
      return false
    }

    const offset = firstChr === 'T' || firstChr === 'G' ? 4 : 0

    ;[2, 7, 6, 5, 4, 3, 2].forEach((elem, i) => {
      weight += parseInt(val.charAt(i + 1), 10) * elem
    })

    const temp = (offset + weight) % 11
    const checkSum = characterSet.charAt(temp)
    return val.charAt(8) === checkSum
  },
  message
})

const isValidUen = (
  message = 'UEN provided is not valid',
  required = false
) => ({
  func: val => {
    if (!val) return !required

    if (val.length === 9) {
      const nineDigitUenRegex = /\d{8}[A-Z]/
      return nineDigitUenRegex.test(val)
    } else if (val.length === 10) {
      const localCompanyAcraRegex = /[12][89012]\d{7}[A-Z]/
      const allOtherCompanyAcraRegex = /[TSR]\d{2}(LP|LL|FC|{F|RF|MQ|MM|NB|CC|CS|MB|FM|GS|DP|CP|NR|CM|CD|MD|HS|VH|CH|MH|CL|XL|CX|RP|TU|TC|FB|FN|PA|PB|SS|MC|SM|GA|GB)\d{4}[A-Z]/
      if (/[0-9]/.test(val[0])) return localCompanyAcraRegex.test(val)
      if (/[TSR]/.test(val[0])) return allOtherCompanyAcraRegex.test(val)
    }
    return false
  },
  message
})

const isValidEmail = (
  message = 'Email provided is not valid',
  required = false
) => ({
  func: val => {
    if (!val) return !required

    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return emailRegex.test(val)
  },
  message: typeof message === 'string' ? message : 'Email provided is not valid'
})

const isValidPhoneNumber = (
  message = 'Phone number provided is not valid',
  required = false
) => ({
  func: val => {
    if (!val) return !required

    // only sg phone numbers for now
    const phoneRegex = /[0-9]{8}/
    return phoneRegex.test(val)
  },
  message:
    typeof message === 'string' ? message : 'Phone number provided is not valid'
})

const isValidDate = (
  message = 'Date provided is not valid',
  required = false
) => ({
  func: val => {
    if (!val) return !required
    return moment(val).isValid()
  },
  message: typeof message === 'string' ? message : 'Date provided is not valid'
})

export {
  isRequired,
  textMatcher,
  isValidNric,
  isValidUen,
  isValidEmail,
  isValidPhoneNumber,
  isValidDate
}
