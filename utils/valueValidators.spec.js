import {
  isRequired,
  textMatcher,
  isValidNric,
  isValidUen,
  isValidEmail
} from './valueValidators'

describe('isRequired', () => {
  it('returns a validator object with default message', () => {
    expect(isRequired()).toEqual({
      func: expect.any(Function),
      message: 'This field is required'
    })
  })
  it('return a validator object with custom message', () => {
    const MESSAGE = 'this is a test'
    expect(isRequired(MESSAGE)).toEqual({
      func: expect.any(Function),
      message: MESSAGE
    })
  })
  it('validator function returns false with an empty string', () => {
    expect(isRequired().func('')).toBe(false)
  })
  it('validator function returns true with a non-empty string', () => {
    expect(isRequired().func('not empty')).toBe(true)
  })
  it('validator function returns true with a number', () => {
    expect(isRequired().func(-123)).toBe(true)
    expect(isRequired().func(0)).toBe(true)
    expect(isRequired().func(123)).toBe(true)
  })
  it('validator function returns true with a boolean', () => {
    expect(isRequired().func(true)).toBe(true)
    // 'false' is still a valid response
    expect(isRequired().func(false)).toBe(true)
  })
  it('validator function returns false with an empty object', () => {
    expect(isRequired().func({})).toBe(false)
  })
  it('validator function returns true with an non-empty object', () => {
    expect(isRequired().func({ hello: 'world' })).toBe(true)
  })
})

describe('textMatcher', () => {
  it('returns a validator object with default message', () => {
    expect(textMatcher()).toEqual({
      func: expect.any(Function),
      message: 'This field does not match the required format'
    })
  })
  it('return a validator object with custom message', () => {
    const MESSAGE = 'this is a test'
    const REGEX = new RegExp('')
    expect(textMatcher(REGEX, MESSAGE)).toEqual({
      func: expect.any(Function),
      message: MESSAGE
    })
  })
  it('should test a regex', () => {
    const REGEX = /[a-z]est[0-9]/
    expect(textMatcher(REGEX).func('test0')).toBe(true)
    expect(textMatcher(REGEX).func('bests')).toBe(false)
    expect(textMatcher(REGEX).func('1est0')).toBe(false)
  })
})

describe('isValidNric', () => {
  it('returns a validator object with default message', () => {
    expect(isValidNric()).toEqual({
      func: expect.any(Function),
      message: 'NRIC provided is not valid'
    })
  })

  it('return a validator object with custom message', () => {
    const MESSAGE = 'this is a test'
    expect(isValidNric(MESSAGE)).toEqual({
      func: expect.any(Function),
      message: MESSAGE
    })
  })

  it('should return true on empty string if not required', () => {
    expect(isValidNric(null, false).func('')).toBe(true)
  })

  it('should return false on empty string if required', () => {
    expect(isValidNric(null, true).func('')).toBe(false)
  })

  const validNRIC = [
    'S7634559G',
    'S1344790Z',
    'S9426732C',
    'S1979583G',
    'S8926625D',
    'T1611784B',
    'T4790671C',
    'T3798640I',
    'T7356015I',
    'T8478867D'
  ]
  it.each(validNRIC)('Should return true from valid NRIC: %s', nric => {
    expect(isValidNric().func(nric)).toBe(true)
  })

  const invalidNRIC = [
    's1344790z',
    'S7699559G',
    'S9499732C',
    'SLKD1999583G',
    'S89KDJ25D',
    'T1699sdfj784b',
    'T4799671C',
    'T32323799640I',
    'T7399015I',
    'T8499867D'
  ]
  it.each(invalidNRIC)('Should return false from invalid NRIC: %s', nric => {
    expect(isValidNric().func(nric)).toBe(false)
  })
})

describe('isValidUen', () => {
  it('returns a validator object with default message', () => {
    expect(isValidUen()).toEqual({
      func: expect.any(Function),
      message: 'UEN provided is not valid'
    })
  })

  it('return a validator object with custom message', () => {
    const MESSAGE = 'this is a test'
    expect(isValidUen(MESSAGE)).toEqual({
      func: expect.any(Function),
      message: MESSAGE
    })
  })

  it('should return true on empty string if not required', () => {
    expect(isValidUen(null, false).func('')).toBe(true)
  })

  it('should return false on empty string if required', () => {
    expect(isValidUen(null, true).func('')).toBe(false)
  })

  const validUenTypeA = ['12341234I', '37482838J', '13848372H', '28374381E']
  it.each(validUenTypeA)(
    'Should return true from valid UEN Type A: %s',
    uen => {
      expect(isValidUen().func(uen)).toBe(true)
    }
  )

  const validUenTypeB = ['198212312E', '20014838J', '19914959E', '20105838M']
  it.each(validUenTypeB)(
    'Should return true from valid UEN Type B: %s',
    uen => {
      expect(isValidUen().func(uen)).toBe(true)
    }
  )

  const validUenTypeC = ['T09LL0001B', 'S99CM1234M', 'R00PA3848J', 'T11FB8438Q']
  it.each(validUenTypeC)(
    'Should return true from valid UEN Type C: %s',
    uen => {
      expect(isValidUen().func(uen)).toBe(true)
    }
  )

  const invalidUEN = [
    '109LL0001B',
    'P99CM1234M',
    '123J3242P',
    '293929381',
    'T09EN0001B'
  ]
  it.each(invalidUEN)('Should return false from invalid UEN: %s', uen => {
    expect(isValidUen().func(uen)).toBe(false)
  })
})

describe('isValidEmail', () => {
  it('returns a validator object with default message', () => {
    expect(isValidEmail()).toEqual({
      func: expect.any(Function),
      message: 'Email provided is not valid'
    })
  })

  it('return a validator object with custom message', () => {
    const MESSAGE = 'this is a test'
    expect(isValidEmail(MESSAGE)).toEqual({
      func: expect.any(Function),
      message: MESSAGE
    })
  })

  it('should return true on empty string if not required', () => {
    expect(isValidEmail(null, false).func('')).toBe(true)
  })

  it('should return false on empty string if required', () => {
    expect(isValidEmail(null, true).func('')).toBe(false)
  })

  const validEmails = [
    'keith@gmail.com',
    'keith@gm.co',
    '2k_ek@2.el',
    '2837438@1E.ce'
  ]
  it.each(validEmails)('Should return true from valid email: %s', email => {
    expect(isValidEmail().func(email)).toBe(true)
  })
  const invalidEmails = [
    'asd',
    'a@co',
    '@.com',
    '@ke.ci',
    '2k_ek@2.1',
    '2837@438@1E.c'
  ]
  it.each(invalidEmails)('Should return false from invalid email: %s', uen => {
    expect(isValidEmail().func(uen)).toBe(false)
  })
})
