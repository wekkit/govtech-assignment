/// <reference types="Cypress" />

const COMPANY_NAME = 'Test Company'
const COMPANY_ADDRESS = '1 Test Way'
const COMPANY_UEN = '28384820X'
const APPLICANT_NAME = 'Tess Terr'
const APPLICANT_PHONE = '91234567'
const APPLICANT_EMAIL = 'tessterr@xyz.com'
const EMPLOYEE_NAME = 'Ginny Pieg'
const EMPLOYEE_NRIC = 'S9187383E'
const EMPLOYEE_PASSPORT = 'E1231234J'
const EMPLOYEE_ORIGIN = 'Russia'
const EMPLOYEE_DESTINATION = 'China'
const EMPLOYEE_DATEFROM = '1 Jan 2020'
const EMPLOYEE_DATETO = '10 Jan 2020'

context('main e2e tests', () => {
  before(() => {
    cy.request('http://localhost:3000/api/reset')
  })
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })

  it.only('creates an application', () => {
    cy.contains('Make an application').click()

    cy.get('#company_name').type(COMPANY_NAME)
    cy.get('#company_address').type(COMPANY_ADDRESS)
    cy.get('#company_uen').type(COMPANY_UEN)
    cy.get('#applicant_name').type(APPLICANT_NAME)
    cy.get('#applicant_phone').type(APPLICANT_PHONE)
    cy.get('#applicant_email').type(APPLICANT_EMAIL)
    cy.get('#employee_name').type(EMPLOYEE_NAME)
    cy.get('#employee_nric').type(EMPLOYEE_NRIC)
    cy.get('#employee_passport').type(EMPLOYEE_PASSPORT)
    cy.get('#employee_origin').type(EMPLOYEE_ORIGIN)
    cy.get('#employee_destination').type(EMPLOYEE_DESTINATION)
    cy.get('#employee_datefrom').type(EMPLOYEE_DATEFROM)
    cy.get('#employee_dateto').type(EMPLOYEE_DATETO)
    cy.contains('Next: Review and Submit').click()

    cy.contains(COMPANY_NAME).should('exist')
    cy.contains(COMPANY_ADDRESS).should('exist')
    cy.contains(COMPANY_UEN).should('exist')
    cy.contains(APPLICANT_NAME).should('exist')
    cy.contains(APPLICANT_PHONE).should('exist')
    cy.contains(APPLICANT_EMAIL).should('exist')
    cy.contains(EMPLOYEE_NAME).should('exist')
    cy.contains(EMPLOYEE_NRIC).should('exist')
    cy.contains(EMPLOYEE_PASSPORT).should('exist')
    cy.contains(EMPLOYEE_ORIGIN).should('exist')
    cy.contains(EMPLOYEE_DESTINATION).should('exist')
    cy.contains('January 1st 2020').should('exist')
    cy.contains('January 10th 2020').should('exist')
    cy.wait(1000)
    cy.contains('Submit').click()

    cy.get('.info').should($info => {
      expect($info).to.have.length(4)
    })
    cy.contains(COMPANY_NAME).should('exist')
    cy.contains(COMPANY_ADDRESS).should('exist')
    cy.contains(COMPANY_UEN).should('exist')
    cy.contains(APPLICANT_NAME).should('exist')
    cy.contains(APPLICANT_PHONE).should('exist')
    cy.contains(APPLICANT_EMAIL).should('exist')
    cy.contains(EMPLOYEE_NAME).should('exist')
    cy.contains(EMPLOYEE_NRIC).should('exist')
    cy.contains(EMPLOYEE_PASSPORT).should('exist')
    cy.contains(EMPLOYEE_ORIGIN).should('exist')
    cy.contains(EMPLOYEE_DESTINATION).should('exist')
    cy.contains('January 1st 2020').should('exist')
    cy.contains('January 10th 2020').should('exist')
  })

  it('shows an error if accessing the confirmation page without filling in the form', () => {
    cy.visit('http://localhost:3000/confirmation')
    cy.contains('Oops!').should('exist')
    cy.contains('Back to application form').click()
    cy.url().should('include', '/application')
  })
})
