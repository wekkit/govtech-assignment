const url = require('url')
const MongoClient = require('mongodb').MongoClient

let cachedDb = null
module.exports = async function connectToDatabase() {
  const uri =
    process.env.NODE_ENV === 'production'
      ? process.env.MONGODB_URI_PROD
      : process.env.MONGODB_URI_DEV
  if (cachedDb) return cachedDb

  const client = await MongoClient.connect(uri, { useNewUrlParser: true })
  const db = await client.db(url.parse(uri).pathname.substr(1))

  cachedDb = db
  return db
}
