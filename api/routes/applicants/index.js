const app = require('../../utils/app')
const connectToDatabase = require('../../utils/db')
const { validateApplicant } = require('../../../utils/entityValidators')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()
  const applicants = await db
    .collection('applicants')
    .find({})
    .toArray()
  res.status(200).json({ applicants })
})

app.post('*', async (req, res) => {
  const db = await connectToDatabase()

  const error = validateApplicant(req.body)
  if (error) return res.status(400).send({ error })

  const newApplicant = await db
    .collection('applicants')
    .insertOne({ name, phone_number: phoneNumber, email })
  return res.status(200).json(newApplicant)
})

module.exports = app
