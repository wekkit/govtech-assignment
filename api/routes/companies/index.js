const app = require('../../utils/app')
const connectToDatabase = require('../../utils/db')
const { validateCompany } = require('../../../utils/entityValidators')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()
  const companies = await db
    .collection('companies')
    .find({})
    .toArray()
  res.status(200).json({ companies })
})

app.post('*', async (req, res) => {
  const db = await connectToDatabase()
  const { name, address, uen } = req.body

  const error = validateCompany(req.body)
  if (error) return res.status(400).send({ error })

  const newCompany = await db
    .collection('companies')
    .insertOne({ name, address, uen })
  return res.status(200).json(newCompany)
})

module.exports = app
