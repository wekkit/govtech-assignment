const app = require('../../utils/app')
const connectToDatabase = require('../../utils/db')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()
  const applications = await db.collection('applications')
  const applicants = await db.collection('applicants')
  const companies = await db.collection('companies')
  const employees = await db.collection('employees')

  const company = {
    name: 'corpotech',
    address: '1 Techno Way',
    uen: 'T09LL0001B'
  }
  const applicant = {
    name: 'Andrew Paul Licant',
    phoneNumber: '91234567',
    email: 'applicant@corpotech.com'
  }
  const employee = {
    name: 'Miguel Ployi',
    nric: 'S9135814J',
    passport: 'E123123J',
    origin: 'Singapore',
    destination: 'China',
    datefrom: '2019-01-01T16:00:00.000+00:00',
    dateto: '2020-01-14T16:00:00.000+00:00'
  }

  await applications.remove({})
  await applicants.remove({})
  await companies.remove({})
  await employees.remove({})
  const newCompany = await companies.insertOne(company)
  const newApplicant = await applicants.insertOne(applicant)
  const newEmployee = await employees.insertOne(employee)
  await applications.insertOne({
    company_id: newCompany.ops[0]._id,
    applicant_id: newApplicant.ops[0]._id,
    employee_id: newEmployee.ops[0]._id,
    status: 'pending'
  })
  await applications.insertOne({
    company_id: newCompany.ops[0]._id,
    applicant_id: newApplicant.ops[0]._id,
    employee_id: newEmployee.ops[0]._id,
    status: 'approved'
  })
  await applications.insertOne({
    company_id: newCompany.ops[0]._id,
    applicant_id: newApplicant.ops[0]._id,
    employee_id: newEmployee.ops[0]._id,
    status: 'rejected'
  })
  res.status(200).send({})
})

module.exports = app
