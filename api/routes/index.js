const app = require('../utils/app')
const connectToDatabase = require('../utils/db')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()

  const companiesCollection = await db.collection('companies')
  const companies = await companiesCollection.find({}).toArray()

  const employeesCollection = await db.collection('employees')
  const employees = await employeesCollection.find({}).toArray()

  const applicantsCollection = await db.collection('applicants')
  const applicants = await applicantsCollection.find({}).toArray()

  const applicationsCollection = await db.collection('applications')
  const applications = await applicationsCollection.find({}).toArray()

  res.status(200).json({ companies, employees, applicants, applications })
})

module.exports = app
