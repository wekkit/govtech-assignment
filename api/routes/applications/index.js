const app = require('../../utils/app')
const connectToDatabase = require('../../utils/db')
const {
  validateCompany,
  validateApplicant,
  validateEmployee
} = require('../../../utils/entityValidators')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()
  const applicationsCollection = await db.collection('applications')
  const applications = await applicationsCollection
    .aggregate([
      {
        $lookup: {
          from: 'companies',
          localField: 'company_id',
          foreignField: '_id',
          as: 'company'
        }
      },
      {
        $lookup: {
          from: 'applicants',
          localField: 'applicant_id',
          foreignField: '_id',
          as: 'applicant'
        }
      },
      {
        $lookup: {
          from: 'employees',
          localField: 'employee_id',
          foreignField: '_id',
          as: 'employee'
        }
      }
    ])
    .toArray()
  const populated = applications.map(application => ({
    _id: application._id,
    company: application.company[0],
    applicant: application.applicant[0],
    employee: application.employee[0],
    status: application.status
  }))
  res.status(200).json({ applications: populated })
})

app.post('*', async (req, res) => {
  const db = await connectToDatabase()
  const { company, applicant, employee } = req.body

  const companyError = validateCompany(company)
  const applicantError = validateApplicant(applicant)
  const employeeError = validateEmployee(employee)

  const error = [companyError, applicantError, employeeError].filter(x => x)
  if (error.length !== 0) return res.status(400).send({ error })

  const newCompany = await db.collection('companies').insertOne(company)
  const newApplicant = await db.collection('applicants').insertOne(applicant)
  const newEmployee = await db.collection('employees').insertOne(employee)
  const newApplication = await db.collection('applications').insertOne({
    company_id: newCompany.ops[0]._id,
    applicant_id: newApplicant.ops[0]._id,
    employee_id: newEmployee.ops[0]._id,
    status: 'pending'
  })

  res.status(200).json({ application: newApplication })
})

module.exports = app
