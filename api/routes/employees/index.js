const app = require('../../utils/app')
const connectToDatabase = require('../../utils/db')
const { validateEmployee } = require('../../../utils/entityValidators')

app.get('*', async (req, res) => {
  const db = await connectToDatabase()
  const employees = await db
    .collection('employees')
    .find({})
    .toArray()
  res.status(200).json({ employees })
})

app.post('*', async (req, res) => {
  const db = await connectToDatabase()
  const {
    name,
    nric,
    passport,
    origin,
    destination,
    datefrom,
    dateto
  } = req.body

  const error = validateEmployee(req.body)
  if (error) return res.status(400).send({ error })

  const employee = await db.collection('employees').insertOne({
    name,
    nric,
    passport,
    origin,
    destination,
    datefrom,
    dateto
  })
  return res.status(200).json(employee)
})

module.exports = app
