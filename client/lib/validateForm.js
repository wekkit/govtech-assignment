export default function(form) {
  const { company, employee, applicant } = form
  const formErrors = [company, employee, applicant].reduce((acc, section) => {
    Object.keys(section).forEach(field => {
      const error = section[field].error
      if (error) acc.push(error)
    })
    return acc
  }, [])
  return formErrors.length === 0
}
