import {
  isRequired,
  isValidUen,
  isValidEmail,
  isValidPhoneNumber,
  isValidNric,
  isValidDate
} from '../../../utils/valueValidators'
import { actionTypes } from '../constants'

const validationSkeleton = {
  company: {
    name: isRequired(),
    address: isRequired(),
    uen: isValidUen()
  },
  applicant: {
    name: isRequired(),
    phoneNumber: isValidPhoneNumber(null, true),
    email: isValidEmail(null, true)
  },
  employee: {
    name: isRequired(),
    nric: isValidNric(null, false),
    passport: isRequired(),
    origin: isRequired(),
    destination: isRequired(),
    datefrom: isValidDate(null, true),
    dateto: isValidDate(null, true)
  }
}

function validateForm(form) {
  Object.keys(form).forEach(section => {
    Object.keys(form[section]).forEach(field => {
      const validator = validationSkeleton[section][field]
        ? validationSkeleton[section][field]
        : { func: () => {}, message: '' }

      form[section][field].error = validator.func(form[section][field].value)
        ? ''
        : validator.message
    })
  })
  return form
}

const initialState = {
  form: validateForm({
    company: {
      name: { value: '' },
      address: { value: '' },
      uen: { value: '' }
    },
    applicant: {
      name: { value: '' },
      phoneNumber: { value: '' },
      email: { value: '' }
    },
    employee: {
      name: { value: '' },
      nric: { value: '' },
      passport: { value: '' },
      origin: { value: '' },
      destination: { value: '' },
      datefrom: { value: '' },
      dateto: { value: '' }
    }
  })
}

export const formReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.RESET_FORM:
      return Object.assign({}, state, { form: initialState.form })

    case actionTypes.UPDATE_FORM:
      const { value, section, field } = action.payload
      const validator = validationSkeleton[section][field]
        ? validationSkeleton[section][field]
        : { func: () => true, message: '' }
      return Object.assign({}, state, {
        form: {
          ...state.form,
          [section]: {
            ...state.form[section],
            [field]: {
              value,
              error: validator.func(value) ? '' : validator.message
            }
          }
        }
      })

    default:
      return state
  }
}

export default formReducer
