import formReducers from './formReducers'
import actionTypes from '../constants'

const initialState = {
  form: {
    company: {
      name: { value: '', error: 'This field is required' },
      address: { value: '', error: 'This field is required' },
      uen: { value: '', error: '' }
    },
    applicant: {
      name: { value: '', error: 'This field is required' },
      phoneNumber: { value: '', error: 'Phone number provided is not valid' },
      email: { value: '', error: 'Email provided is not valid' }
    },
    employee: {
      name: { value: '', error: 'This field is required' },
      nric: { value: '', error: '' },
      passport: { value: '', error: 'This field is required' },
      origin: { value: '', error: 'This field is required' },
      destination: { value: '', error: 'This field is required' },
      datefrom: { value: '', error: 'Date provided is not valid' },
      dateto: { value: '', error: 'Date provided is not valid' }
    }
  }
}

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(formReducers(undefined, {})).toEqual(initialState)
  })

  it('should handle UPDATE_FORM', () => {
    expect(
      formReducers(undefined, {
        type: actionTypes.UPDATE_FORM,
        payload: { value: 'hello', section: 'company', field: 'name' }
      })
    ).toEqual({
      form: {
        ...initialState.form,
        company: {
          ...initialState.form.company,
          name: { value: 'hello', error: '' }
        }
      }
    })
  })

  it('should revalidate a new error', () => {
    expect(
      formReducers(undefined, {
        type: actionTypes.UPDATE_FORM,
        payload: { value: 'invalid', section: 'company', field: 'uen' }
      })
    ).toEqual({
      form: {
        ...initialState.form,
        company: {
          ...initialState.form.company,
          uen: { value: 'invalid', error: 'UEN provided is not valid' }
        }
      }
    })
  })

  it('should handle RESET_FORM', () => {
    expect(
      formReducers(
        {
          form: {
            ...initialState.form,
            company: {
              ...initialState.form.company,
              name: { value: 'hello', error: '' }
            }
          }
        },
        {}
      )
    ).not.toEqual(initialState)
    expect(
      formReducers(
        {
          form: {
            ...initialState.form,
            company: {
              ...initialState.form.company,
              name: { value: 'hello', error: '' }
            }
          }
        },
        { type: actionTypes.RESET_FORM }
      )
    ).toEqual(initialState)
  })
})
