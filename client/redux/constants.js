export const actionTypes = {
  UPDATE_FORM: 'UPDATE_FORM',
  SUBMIT_FORM: 'SUBMIT_FORM',
  RESET_FORM: 'RESET_FORM'
}

export default actionTypes
