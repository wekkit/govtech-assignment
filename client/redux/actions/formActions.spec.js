import actions from './formActions'
import actionTypes from '../constants'

describe('form actions', () => {
  it('should create an action to update the form', () => {
    const text = 'testing'
    const expectedAction = {
      type: actionTypes.UPDATE_FORM,
      payload: text
    }
    expect(actions.updateForm(text)).toEqual(expectedAction)
  })

  it('should create an action to reset the form', () => {
    const expectedAction = {
      type: actionTypes.RESET_FORM
    }
    expect(actions.resetForm()).toEqual(expectedAction)
  })
})
