import actionTypes from '../constants'

export const updateForm = payload => {
  return { type: actionTypes.UPDATE_FORM, payload: payload }
}

export const resetForm = () => {
  return { type: actionTypes.RESET_FORM }
}

export default { updateForm, resetForm }
