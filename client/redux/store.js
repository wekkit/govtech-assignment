import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createLogger } from 'redux-logger'
import { formReducer } from './reducers/formReducers'

const logger = createLogger({
  collapsed: true
})

export function initializeStore(initialState = initialState) {
  return createStore(
    formReducer,
    initialState,
    composeWithDevTools(applyMiddleware(logger))
  )
}
