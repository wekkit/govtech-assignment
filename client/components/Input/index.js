import React, { useState } from 'react'

export default function Input(props) {
  const {
    initalValue = '',
    name,
    id,
    label,
    error,
    value,
    pristine: pristineProp,
    onChange = () => {},
    onBlur = () => {},
    ...otherProps
  } = props

  const [pristine, setPristine] = useState(true)
  function handleBlur(e) {
    if (pristine === true) setPristine(false)
    onBlur(e)
  }

  let showError = true
  if (pristine === true) showError = false
  if (pristineProp === false) showError = true

  return (
    <div className="container">
      {label && <label htmlFor={id || name || label}>{label}</label>}
      <input
        className={error && showError ? 'error' : ''}
        name={name}
        id={id || name || label}
        type="text"
        value={value}
        onBlur={handleBlur}
        onChange={onChange}
        {...otherProps}
      />
      {error && showError && <p className="errorText">{error}</p>}

      <style jsx="true">{`
        .container {
          min-width: 26%;
        }
        label {
          margin: 10px 10px 5px 0;
          display: block;
        }
        input {
          font-size: 16px;
          padding: 10px;
          width: 250px;
          border: none;
          background: rgb(0, 0, 0, 0);
          border-bottom: 1px solid grey;
          transition: all 0.3s;
          margin-bottom: 30px;
        }
        input:hover {
          border-bottom: 2px solid black;
        }
        input:focus,
        input:active {
          outline: none;
          background: rgb(0, 0, 0, 0.05);
        }
        input.error {
          border-bottom: 1px solid red;
          margin-bottom: 0;
        }
        .errorText {
          font-size: 12px;
          margin: 10px 0 0;
          color: red;
        }
      `}</style>
    </div>
  )
}
