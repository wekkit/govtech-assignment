import React from 'react'
import { render, cleanup, fireEvent } from '@testing-library/react'
import Input from './index'
import 'jest-dom/extend-expect'

afterEach(cleanup)

it('renders an input', async () => {
  const { getByText, container } = render(<Input label="hello" name="name" />)
  expect(container).toMatchSnapshot()
  expect(getByText('hello')).toHaveTextContent('hello')
})

it('has a corresponding input and label', () => {
  const { getByLabelText } = render(<Input label="hello" name="name" />)
  expect(getByLabelText('hello')).toBeTruthy()
})

it('displays the value prop provided', () => {
  const { getByDisplayValue } = render(<Input label="hello" value="world" />)
  expect(getByDisplayValue('world')).toBeTruthy()
})

it('does not display the error prop provided when pristine', () => {
  const { getByText } = render(<Input label="hello" error="this is an error" />)
  expect(() => getByText('this is an error')).toThrowError()
})

it('displays the error prop provided when pristine is removed', () => {
  const { getByText, getByLabelText } = render(
    <Input label="hello" error="this is an error" />
  )
  expect(() => getByText('this is an error')).toThrowError()
  fireEvent.blur(getByLabelText('hello'))
  expect(getByText('this is an error')).toBeTruthy()
})

it('displays the error prop provided when unpristineness is declared', () => {
  const { getByText } = render(
    <Input label="hello" error="this is an error" pristine={false} />
  )
  expect(getByText('this is an error')).toBeTruthy()
})

it('should run the onChange callback', () => {
  const onChange = jest.fn()
  const { getByLabelText, getByDisplayValue } = render(
    <Input label="label" onChange={onChange} />
  )
  fireEvent.change(getByLabelText('label'), { target: { value: 'hello' } })
  expect(onChange).toHaveBeenCalled()
  expect(getByDisplayValue('hello')).toBeTruthy()
})
