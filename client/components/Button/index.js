import React from 'react'

export default function Button(props) {
  const { label = '', children, ...otherProps } = props
  return (
    <button {...otherProps}>
      {label || children}
      <style jsx="true">{`
        button {
          color: white;
          background: #1565c0;
          border: 1px solid rgb(255, 255, 255, 0.3);
          padding: 10px 20px;
          font-size: 1rem;
          cursor: pointer;
          transition: all 0.3s;
        }
        button:hover,
        button:focus {
          outline: 1px solid #1565c0;
          color: #1565c0;
          background: white;
          border: 1px solid #1565c0;
        }
      `}</style>
    </button>
  )
}
