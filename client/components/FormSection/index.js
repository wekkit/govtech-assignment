import React from 'react'

export default function FormSection(props) {
  const { header, children } = props
  return (
    <div className="container">
      <h1>{header}</h1>
      <div className="inputs">{children}</div>
      <style jsx="true">{`
        .container {
          margin: 20px 0;
        }
        .inputs {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
      `}</style>
    </div>
  )
}
