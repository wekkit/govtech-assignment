import React, { useState } from 'react'
import moment from 'moment'
import { MOBILE_BREAKPOINT } from '../../lib/constants'
import { CSSTransition } from 'react-transition-group'

export default function Application(props) {
  const { preview } = props
  const [expanded, setExpanded] = useState(preview ? true : false)
  const { application } = props
  const { company, applicant, employee, status } = application
  let statusColor
  switch (status) {
    case 'pending':
      statusColor = '#FFEE58'
      break
    case 'approved':
      statusColor = '#66BB6A'
      break
    case 'rejected':
      statusColor = '#ef5350'
      break
  }

  return (
    <div
      onClick={() => {
        if (!preview) setExpanded(!expanded)
      }}
      className="container"
    >
      <div className="details">
        <table>
          <tbody>
            <tr>
              <td>Country of origin:</td>
              <td>{employee.origin}</td>
            </tr>
            <tr>
              <td>Country of destination:</td>
              <td>{employee.destination}</td>
            </tr>
          </tbody>
        </table>
        <table>
          <tbody>
            <tr>
              <td>From:</td>
              <td>{moment(employee.datefrom).format('MMMM Do YYYY')}</td>
            </tr>
            <tr>
              <td>To:</td>
              <td>{moment(employee.dateto).format('MMMM Do YYYY')}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <CSSTransition
        in={expanded}
        timeout={{ enter: 300, exit: 300 }}
        classNames="info"
        unmountOnExit
      >
        <div className="info">
          <div className="section">
            <h3>
              <span className="sub">Company</span>
              {company.name}
            </h3>
            <table>
              <tbody>
                <tr>
                  <td>Address</td>
                  <td>{company.address}</td>
                </tr>
                <tr>
                  <td>UEN</td>
                  <td>{company.uen || '-'}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="section">
            <h3>
              <span className="sub">Applicant</span>
              {applicant.name}
            </h3>
            <table>
              <tbody>
                <tr>
                  <td>Phone Number</td>
                  <td>{applicant.phoneNumber}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>{applicant.email}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="section">
            <h3>
              <span className="sub">Employee</span>
              {employee.name}
            </h3>
            <table>
              <tbody>
                <tr>
                  <td>NRIC:</td>
                  <td>{employee.nric || '-'}</td>
                </tr>
                <tr>
                  <td>Passport Number:</td>
                  <td>{employee.passport}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </CSSTransition>
      {status && <span className="status">Status: {status}</span>}

      <style jsx="true">{`
        .container {
          background: rgb(255, 255, 255, 0.3);
          padding: 20px 10px;
          margin: 20px auto;
          ${preview ? '' : 'cursor: pointer;'}
          transition: background 0.3s;
        }
        .container:hover {
          ${preview ? '' : 'background: rgb(255, 255, 255, 0.5);'}
        }
        .details {
          display: flex;
          margin-bottom: 10px;
        }
        .details tr td:first-of-type {
          width: 50%;
        }
        .info {
          display: flex;
          justify-content: space-between;
          transition: all 0.5s;
        }
        .info-enter {
          opacity: 0;
          transform: scale(1, 0.9);
          height: 0;
          min-height: 0;
        }
        .info-enter-active {
          opacity: 1;
          transform: translateX(0);
          min-height: 150px;
          transition: opacity 100ms, transform 300ms, min-height: 300ms;
        }
        .info-exit {
          opacity: 1;
          min-height: 150px
        }
        .info-exit-active {
          opacity: 0;
          transform: scale(1, 0.9);
          min-height: 0;
          height: 0;
          transition: opacity 100ms, transform 300ms, min-height: 300ms;
        }
        .section {
          width: 30%;
        }
        .section p {
          margin: 0;
        }
        .section h3 {
          margin: 10px 10px 10px 0;
          padding-bottom: 5px;
          border-bottom: 1px solid black;
        }
        table {
          width: 100%;
        }
        td {
          padding: 5px;
        }
        tr:nth-of-type(2n) {
          background: rgb(255, 255, 255, 0.3);
        }
        .sub {
          font-size: 10px;
          margin: 0 10px;
        }
        .status {
          position: absolute;
          right: 10px;
          font-size: 14px;
          background: ${statusColor};
          padding: 3px 10px;
          border-radius: 8px;
        }

        @media screen and (max-width: ${MOBILE_BREAKPOINT}) {
          .fromTo {
            width: 100%;
          }
          .info {
            flex-direction: column;
          }
          .section {
            width: 100%;
            margin: 10px auto;
          }
          .details {
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  )
}
