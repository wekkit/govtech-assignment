import React, { useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateForm } from '../redux/actions/formActions'
import Router from 'next/router'
import { MOBILE_BREAKPOINT } from '../lib/constants'
import validateForm from '../lib/validateForm'

import Input from '../components/Input'
import FormSection from '../components/FormSection'
import Button from '../components/Button'

function ApplicationPage(props) {
  const [pristine, setPristine] = useState(true)
  const { form, updateForm } = props
  const { company, employee, applicant } = form

  const updateCompany = field => e =>
    updateForm({ value: e.target.value, section: 'company', field })

  const updateApplicant = field => e =>
    updateForm({ value: e.target.value, section: 'applicant', field })

  const updateEmployee = field => e =>
    updateForm({ value: e.target.value, section: 'employee', field })

  return (
    <div>
      <form>
        <FormSection header="Company details">
          <Input
            id="company_name"
            pristine={pristine}
            label="Name of company*"
            value={company.name.value}
            error={company.name.error}
            onChange={updateCompany('name')}
          />
          <Input
            id="company_address"
            pristine={pristine}
            label="Address of company*"
            value={company.address.value}
            error={company.address.error}
            onChange={updateCompany('address')}
          />
          <Input
            id="company_uen"
            pristine={pristine}
            label="UEN of company"
            value={company.uen.value}
            error={company.uen.error}
            onChange={updateCompany('uen')}
          />
        </FormSection>
        <FormSection header="Applicant details">
          <Input
            id="applicant_name"
            label="Name of applicant*"
            value={applicant.name.value}
            error={applicant.name.error}
            pristine={pristine}
            onChange={updateApplicant('name')}
          />
          <Input
            id="applicant_phone"
            label="Phone number of applicant*"
            value={applicant.phoneNumber.value}
            error={applicant.phoneNumber.error}
            pristine={pristine}
            onChange={updateApplicant('phoneNumber')}
          />
          <Input
            id="applicant_email"
            label="Email of applicant*"
            value={applicant.email.value}
            error={applicant.email.error}
            pristine={pristine}
            onChange={updateApplicant('email')}
          />
        </FormSection>
        <FormSection header="Employee (traveller) details">
          <Input
            id="employee_name"
            label="Name of employee*"
            value={employee.name.value}
            error={employee.name.error}
            pristine={pristine}
            onChange={updateEmployee('name')}
          />
          <Input
            id="employee_nric"
            label="NRIC/FIN of employee"
            value={employee.nric.value}
            error={employee.nric.error}
            pristine={pristine}
            onChange={updateEmployee('nric')}
          />
          <Input
            id="employee_passport"
            label="Passport number of employee*"
            value={employee.passport.value}
            error={employee.passport.error}
            pristine={pristine}
            onChange={updateEmployee('passport')}
          />
          <div className="travelDetails">
            <h2>Travel details</h2>
            <div>
              <div>
                <Input
                  id="employee_origin"
                  label="Country of origin*"
                  value={employee.origin.value}
                  error={employee.origin.error}
                  pristine={pristine}
                  onChange={updateEmployee('origin')}
                />
                <Input
                  id="employee_destination"
                  label="Country of destination*"
                  value={employee.destination.value}
                  error={employee.destination.error}
                  pristine={pristine}
                  onChange={updateEmployee('destination')}
                />
              </div>
              <div>
                <Input
                  id="employee_datefrom"
                  label="Travel Period (From)*"
                  value={employee.datefrom.value}
                  error={employee.datefrom.error}
                  pristine={pristine}
                  onChange={updateEmployee('datefrom')}
                />
                <Input
                  id="employee_dateto"
                  label="Travel Period (To)*"
                  value={employee.dateto.value}
                  error={employee.dateto.error}
                  pristine={pristine}
                  onChange={updateEmployee('dateto')}
                />
              </div>
            </div>
          </div>
        </FormSection>

        <Button
          type="submit"
          onClick={e => {
            e.preventDefault()
            setPristine(false)
            if (validateForm(form)) Router.push('/confirmation')
          }}
        >
          Next: Review and Submit
        </Button>
      </form>

      <style jsx="true">{`
        .travelDetails {
          width: 100%;
        }
        .travelDetails > div {
          display: flex;
          flex-direction: row;
          justify-content: space-around;
        }
        .travelDetails > div > div {
          display: flex;
          flex-direction: column;
        }

        @media screen and (max-width: ${MOBILE_BREAKPOINT}) {
          .travelDetails > div {
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  )
}

function mapStateToProps(state) {
  const { form } = state
  return { form }
}
const mapDispatchToProps = dispatch =>
  bindActionCreators({ updateForm }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationPage)
