import App, { Container } from 'next/app'
import Head from 'next/head'
import Link from 'next/link'
import React from 'react'
import withReduxStore from '../lib/withReduxStore'
import { Provider } from 'react-redux'

class MyApp extends App {
  render() {
    const { Component, pageProps, reduxStore } = this.props
    return (
      <Container>
        <Provider store={reduxStore}>
          <Head>
            <title>XYZ Travel</title>
          </Head>
          <header>
            <Link href="/">
              <a>
                <img src="/static/xyzlogo.png" />
                XYZ
              </a>
            </Link>
          </header>
          <div className="container">
            <Component {...pageProps} />
          </div>
        </Provider>
        <style global="true" jsx="true">{`
          @import url('https://fonts.googleapis.com/css?family=Roboto|Rubik+Mono+One&display=swap');
          body {
            font-family: 'Roboto', sans-serif;
            background-image: linear-gradient(to right, #b3e5fc, #bbdefb 60%);
            color: #37474f;
            padding: 0;
            margin: 0;
          }
        `}</style>
        <style jsx="true">{`
          header {
            position: fixed;
            top: 0;
            width: 100vw;
            height: 40px;
            background: rgba(255, 255, 255, 0.7);
            padding: 10px;
            margin: 0;
          }
          header img {
            height: 100%;
          }
          a {
            display: flex;
            flex-direction: row;
            align-items: center;
            text-decoration: none;
            font-family: 'Rubik Mono One', serif;
            height: 100%;
            font-size: 24px;
          }
          a:visited {
            color: #1565c0;
          }
          .container {
            padding: 50px 20px 20px;
            max-width: 1280px;
            margin: 0 auto;
          }
        `}</style>
      </Container>
    )
  }
}

export default withReduxStore(MyApp)
