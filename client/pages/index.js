import React from 'react'
import Link from 'next/link'
import Button from '../components/Button'

export default function HomePage(props) {
  return (
    <div className="container">
      <img src="static/xyzlogo.png" />
      <h1>Ready to globalize?</h1>
      <p>
        XYZ Travel Solutions is the leading force in sending people all over the
        world, for all sorts of purposes. From London to Timbuktu, our mission
        is to get you where you need to go, with no fuss and no mess.
      </p>
      <p>So what are you waiting for?</p>
      <Link href="/application">
        <a>
          <Button>Make an application</Button>
        </a>
      </Link>
      <Link href="/dashboard">
        <a>
          <Button>See applications</Button>
        </a>
      </Link>

      <style jsx="true">{`
        .container {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
        }
        img {
          max-width: 80vw;
        }
        a {
          margin: 10px auto;
        }
        p {
          max-width: 500px;
        }
      `}</style>
    </div>
  )
}
