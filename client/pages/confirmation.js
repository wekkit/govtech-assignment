import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { resetForm } from '../redux/actions/formActions'
import Link from 'next/link'
import Router from 'next/router'
import Modal from 'react-modal'
import validateForm from '../lib/validateForm'
import Button from '../components/Button'
import Application from '../components/Application'
Modal.setAppElement('#__next')

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column'
  }
}

function stripErrors(state) {
  return Object.keys(state).reduce((acc, section) => {
    acc[section] = Object.keys(state[section]).reduce((acc2, field) => {
      acc2[field] = state[section][field].value
      return acc2
    }, {})
    return acc
  }, {})
}

function Confirmation(props) {
  const { form, resetForm } = props
  const formValues = stripErrors(form)
  const [showError, setError] = useState(false)

  async function handleSubmit() {
    const res = await fetch('/api/applications', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formValues)
    })
    if (res.status === 200) {
      resetForm()
      return Router.push('/dashboard')
    } else {
      setError(true)
    }
  }

  useEffect(() => {
    if (!validateForm(form)) setError(true)
  }, [])

  return (
    <div>
      <h1>Please review your application details:</h1>
      <Application preview application={formValues} />
      <Link href="/application">
        <a>
          <Button>Back</Button>
        </a>
      </Link>
      <Button onClick={handleSubmit}>Submit</Button>

      <Modal isOpen={showError} style={customStyles} contentLabel="form error">
        <h1>Oops!</h1>
        <h2>Looks like something went wrong.</h2>
        <p>Please review your application and try again!</p>
        <Button onClick={() => Router.push('/application')}>
          Back to application form
        </Button>
      </Modal>
    </div>
  )
}

function mapStateToProps(state) {
  const { form } = state
  return { form }
}
const mapDispatchToProps = dispatch =>
  bindActionCreators({ resetForm }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Confirmation)
