import React, { useState, useEffect } from 'react'
import fetch from 'isomorphic-unfetch'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Link from 'next/link'
import Loading from '../components/Loading'
import Application from '../components/Application'
import Button from '../components/Button'

function Dashboard() {
  const [applications, setApplications] = useState([])
  const [isLoaded, setLoaded] = useState(false)
  useEffect(() => {
    async function fetchData() {
      const res = await fetch('/api/applications')
      const fetched = await res.json()
      setApplications(fetched.applications)
      setLoaded(true)
    }
    fetchData()
  }, [])

  const [fadeInCounter, setFadeInCounter] = useState(0)
  useEffect(() => {
    if (!isLoaded) return
    if (fadeInCounter === applications.length) return
    setTimeout(() => {
      setFadeInCounter(fadeInCounter + 1)
    }, 100)
  }, [fadeInCounter, isLoaded])

  return (
    <div>
      <h1>Applications</h1>

      <Link href="/application">
        <a>
          <Button>Make a new application</Button>
        </a>
      </Link>
      {isLoaded ? (
        applications.map((application, i) => {
          return (
            <div
              key={application._id}
              className={i < fadeInCounter ? 'container off' : 'container on'}
            >
              <Application application={application} />
            </div>
          )
        })
      ) : (
        <Loading />
      )}

      <style jsx="true">{`
        .container {
          position: relative;
          transition: all 0.5s;
        }
        .on {
          top: 100px;
          opacity: 0;
        }
        .off {
          top: 0;
          opacity: 1;
        }
      `}</style>
    </div>
  )
}

function mapStateToProps(state) {
  return state
}
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)
