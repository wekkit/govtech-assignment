
# Component hierarchy
Most of the state logic has been compartmentalized into redux, which means that most of the components are flat, display-only components, with little need for complex prop-drilling or internal state logic. Most components, like `Application` or `FormSection`, are meant to simply take in display values and render them in the DOM with styling in a reusable fashion.

Of the components used in this project only one handles input events and requires complex user interaction logic - the `Input` component. Most of the focus on the client was concentrated on this component, but ultimately the internal logic is mostly pertaining to pristineness (See below). The `Input` component also primary just takes in a value, a label, an error etc. and displays then accordingly. Changes to these values are handled via updates to the redux store and reflected accordingly via the new transformed state.

# State management

State management within the client is done via [redux](https://redux.js.org/). Several different state managment solutions were considered for this project, including:
- [React Context + Hooks](https://medium.com/simply/state-management-with-react-hooks-and-context-api-at-10-lines-of-code-baf6be8302c), which would reduce depencies on a third-party solution and keep most code within React
- [MobX](https://mobx.js.org), which would allow exploration of a more unconventional decorator API and make use of Observables
- Rolling my own state management using closures

Ultimately, redux was chosen due to its well-known flux pattern, ecosystem of plugins, comprehensive documentation, and community support, making it an ideal solution for future extensibility.

![redux architecture](./redux-architecture.jpg)
*A generic diagram of the redux architecture. Credit to [@andrestaltz](https://twitter.com/andrestaltz).*

# Validation

Validation for fields is an integral part of the application filing process, and vital to good UX. Validation for this project is done as part of the `UPDATE_FORM` action.

Whenever an input is changed and the action is called, the value is updated in the redux store, and validation is applied. Validators are stored as part of a validation tree which mirrors the form state - looking up the validator of a field is the same as looking up where in the store an update is applied, just in the validator tree instead.

 Validators have two properties - the `func` that is run against the value, which returns a boolean if the value is invalid or not, and the corresponding error message if the validation fails. If the `validator.func` returns `false`, then the corresponding `validator.message` is also put into the store as part of the `UPDATE_FORM` action.

![validations](./validators.png)

## Pristineness
Pristineness is also an important part of good form UX. A pristine input means that has not yet been interacted with by the user, and thus should not display any errors, even if the value of the input is not valid. This is to not blame the user for errors that were the fault of the user themselves. There are two ways in which pristiness can be removed - via interacting with the input component, and by submitting the form. 

In this project, internal pristineness is removed when the first `blur` event is fired, allowing the user one full attempt at filling in a field before displaying errors. This is opposed to on a `change` event, which will prematurely show errors while the value is still in transitive state. Another solution is to debounce error rendering, which was not explored due to time constraints.

In the case of submitting a form, external pristineness is handled via overriding internal pristineness through the input's `pristine` prop. If `pristine={false}` is passed in as a prop, then it is declared that the input *must* render errors, if any, even if internally the input is pristine. Thus, error rendering can be handled externally via the `onSubmit` method, or any other means that require it with the use of prop manipulation.
