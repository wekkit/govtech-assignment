The scripts for running the tests are:
- `npm run test:spec` for unit/integration tests using Jest
- `npm run test:e2e` for end-to-end tests using cypress
- `npm run test` for a full run of jest and cypress tests

Additionally,
- `npm run test:spec:watch` to run Jest in watch mode
- `npm run cypress` to open the cypress dashboard

# Test strategy
The following test frameworks were used to carry out a basic testing pyramid (unit >  integration > end-to-end) to ensure code quality while avoiding a debilitating level of brittleness.

- [React Testing Library](https://testing-library.com/) was used for testing the DOM via  React components.
- [Jest](https://jestjs.io) was used as the test runner for all non-e2e tests.
- [Cypress](https://www.cypress.io/) was used for running automated browser end-to-end tests for happy-path scenarios and major error UX paths.

![testing strategy](./testing-strategy.png)

These tests are run as pre-commit and pre-push hooks to ensure that previous functionality is not compromised whenever new functionality is developed and committed to the codebase.

# Systems under test

## Unit testing
These systems were tested at the unit level, as individual pieces of code with little/no dependencies:
- Single-value validators (e.g. `isValidNric`), which return a boolean indicating if the provided value is invalid or not
- Redux reducers, which transforms a given state into another state
- Display components, which transforms props into DOM elements

Due to time constraints, most of the component testing was done on the `Input` component. This was due to the it being the most functionally vital component, with the rest of the components displaying provided information via props.

## Integration testing 


Due to time constraints, less time was given to testing at the integration level. In general, this was due to a focus on unit testing during development time, then implementation of e2e tests for smoke testing. 

However, tests were done on entity validators, which were composed of value validators for individual fields. This worked to ensure that entity validation would work in server controllers. Since most of the server controllers were essentially just passing data between the client and the database, while making sure that said data was valid, the validators made up most of the coverage of server-side testing.

## End-to-end testing

Basic end-to-end testing using Cypress was done to ensure that the happy path does not break during development. This involves the creation of a new application, submitting it, and asserting that the new application can be found on the dashboard.

Note that these tests are buggy and have a high rate of false negatives, so they have been taken out of the commit/push hooks - Due to time constraints, the cause of this bug was not found.