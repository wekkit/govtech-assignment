This repo is my attempt on the take-home assignment for GovTech. The brief of the assignment can be found [here](./docs/brief.md). The production application can be found hosted [here](https://govtech-test.wekkit.now.sh). 

To run locally, you need to install the [now cli](https://github.com/zeit/now-cli) and run `now dev` or `npm run dev` *in the root of the project directory*. The entire application (client and server) needs to be run simultaneously using now's dev environment. This will be handled via `now dev`'s configuration via the `now.json` file. [Read more here.](https://zeit.co/blog/now-dev) There is also an `.env` file required with access crendentials to the mongoDB Atlas database. Please get these credentials from a [developer already working on this project](https://telegram.me/wekkit).

The stack for this application is split into two parts (not including the database) - the browser application (`/client`) and the server (`/api`). The application is built and run using [now](https://zeit.co/now), which uses different loaders for each of the client and api environments: it uses the `nextjs` builder for the client and the `node` builder for the api. Each `.js` file in the `api/routes` and the `client/pages` folders are transformed into routes with their own lambdas using now - so each API endpoint and page route are run as [serverless lambdas](https://zeit.co/docs/v2/deployments/concepts/lambdas).

![app architecture](./docs/architecture.png)

# Client
The front-end framework used for the client-side application is [Next.js](https://nextjs.org/), which is built around React. Next.js is a React framework for server-side rendering web apps. State management within the client is done via [redux](https://redux.js.org/). You can read more about the client architecture in detail [here](./docs/fe-architecture.md).

A basic testing strategy (unit, integration, end-to-end) was used to ensure code quality while avoiding a debilitating level of brittleness. The following frameworks/tools were used to achieve this: 

- [Jest](https://jestjs.io)
- [React Testing Library](https://testing-library.com/)
- [Cypress](https://www.cypress.io/)

To read more about the testing strategy, read [here](./docs/testing.md).

# Server

The server code in the `/api` directory leverages [express](https://expressjs.com) in conjunction with now's `node` builder API.

Using now's node builder, the `api/routes` directory corresponds to routes in the `xyz.com/api` endpoint. For example the file `api/routes/companies.js` exporting a express app will create an API endpoint on `xyz.com/api/companies` when hosted in now. Each entity in the database (applications, companies, applicants, employees) each has its own endpoint with a `GET` and `POST` endpoint to retreive and create data in the database. 

As this is a front-end exercise, the structure of the APIs (and the backend in general) is not structured with as much care as a real production application would be, but it was a good exercise in quick development of a semi-serious project nonetheless.

# Database

For this exercise, I opted to go with [MongoDB Atlas](https://www.mongodb.com/cloud/atlas), a backend-as-a-service platform.

Several approaches were considered for a database solution for this exercise, including:
- [Firebase](https://firebase.google.com), another BaaS
- [An API server](https://github.com/typicode/json-server)
- Stubbed endpoints
- Hosting my own MongoDB or PostgreSQL in Heroku, AWS, etc.

Ultimately, I decided to go with Atlas as it was a platform that I had not used before. The JSON server was a good option to get going quickly, but it's structure was not compatible with now's serverless structure. Firebase was also a fair option to quickly get started, including tools for authentication, but I had used it before and decided to opt for something new to expand my skills and toolkit. The experience of learning to use Atlas was rewarding and paid off in the end.

As this is a front-end exercise, I did not put a great deal of thought into the structure of the collections and documents in the mongoDB database, opting for the 'brute force' structure of each entity having its own collection, and referencing between collections via ObjectIDs.

# Extensibility

This project was built in a way to be easily extensible. Some of the principles that factored in to tooling and architecture:

- Low amount of interdependencies, but shared logic where applicable
- Good framework of testing that is robust but reliant
- Built on frameworks/tools that are well documented and supported

# Stretch Goals

- Integration test coverage on server controllers
- User account authentication with [Auth0](https://auth0.com)
- Use [hooks API](https://react-redux.js.org/next/api/hooks) for redux
- Refactor API calls into redux actions with [redux-thunk](https://github.com/reduxjs/redux-thunk)
- Filter/sorting of applications on dashboard
- Better date support on datefrom/dateto fields
- Usage of typescript for better developer experience and robustness of data